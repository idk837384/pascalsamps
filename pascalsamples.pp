program SampleProgs;

(*Created by idk837384, 2023*)

procedure TruthMachine();
(*A truth machine procedure. It gets a number from the user, and if the input is 1 it will
print 1 infinitely, or if the input it 0 it will print 0 once and terminate.*)
var 
    Num: Char;
begin
    Readln(Num); (*Get user input*)
    if Num = '0' then
        Writeln('0') (*If input is 0, print 0 and terminate*)
    else if Num = '1' then
        while 1 <> 2 do
	begin
	    Writeln('1') (*Print 1 infinitely as the input was 1*)
        end; 
end;

procedure BotsOfBeer();
(*A bottles of beer procedure, it prints out the lyrics to 99 Bottles of Beer 
on the Wall*)
var 
    Bottles: Integer;
    Bottless: Integer;
begin
    Bottles := 99;
    Bottless := 98;
    while Bottles > 0 do (*Loop to keep printing until the program reaches 0 bottles of beer*)
    begin
        Writeln(Bottles, ' bottles of beer on the wall, ', Bottles, ' of beer!'^M^J'If one of those bottles should happen to fall,'^M^J'', Bottless, ' bottles of beer!');
	Bottles := Bottles - 1;
	Bottless := Bottless - 1;
    end;
end;

procedure GuessMyNumber();
(*This program will generate a random number between 0 and 9, then get the user to guess
it until they get it right*)
var
    Numint: Integer;
    Guess: String;
    Num: String;
begin
    randomize(); (*Make the random num generator work well*)
    Numint := random(10) - 1;
    Str(Numint, Num); (*Convert number to guess into a string to be compared against user guess*)
    Writeln('Im thinking of a number between 0 and 9, can you guess what it is?');
    while Guess <> Num do
    begin
        Readln(Guess);
	if Guess <> Num then
	    Writeln('WRONG. Try again!') (*Check if the number was right, otherwise get another guess*)
        else
	    Writeln('Correct, the number was ', Guess);
    end;
end;

var
    Progtorun: Char;
begin
	(*Here the program lets the user running it to select which of the 3 programs to run*)
	Writeln('There are 3 sample programs to select from:'^M^J'1 - Truth Machine'^M^J'2 - Bottles of Beer'^M^J'3 - Guess my Numner');
	Readln(Progtorun);
	if Progtorun = '1' then
	    TruthMachine()
	else if Progtorun = '2' then
	    BotsOfBeer()
        else if Progtorun = '3' then
	    GuessMyNumber();
end.
